# Reading dotenv
DOTENV ?= .env
ifneq (, $(DOTENV))
ifneq (,$(wildcard ./$(DOTENV)))
	include $(DOTENV)
	export
endif
endif

# GO Options
TAGS      	?=
TEST_TAGS   ?=
GOFLAGS    	?=
EXT_LDFLAGS ?=
LDFLAGS      = -w -s $(EXT_LDFLAGS)
SRC        	:= $(shell find . -type f -name '*.go' -print)
# rules

.PHONY: build
build: $(SRC)
	go build $(GOFLAGS) -tags '$(TAGS)' -ldflags '$(LDFLAGS)' ./...

.PHONY: test
test:
	go test ./... -count=1 -v -tags '$(TEST_TAGS)'

.PHONY: lint
lint:
	golangci-lint run
