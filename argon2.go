// Package argon2 contains methods to hash and validate a password
package argon2

import (
	"crypto/subtle"
	"encoding/base64"
	"errors"
	"fmt"
	"strings"

	"golang.org/x/crypto/argon2"
)

// Variant is the argon2 variant
type Variant string

// Possible values for variants
const (
	Variant2ID Variant = "argon2id"
	Variant2I  Variant = "argon2i"
)

type argon2Hasher func(password, salt []byte, time, memory uint32, threads uint8, keyLen uint32) []byte

// variants maps the variant to its hasher
var variants = map[Variant]argon2Hasher{Variant2ID: argon2.IDKey, Variant2I: argon2.Key}

// Params has all the params
type Params struct {
	Variant     Variant
	SaltLen     uint32
	Memory      uint32
	Time        uint32
	Parallelism uint8
	KeyLen      uint32

	salt []byte
}

// HashPassword generates a pwd hash using specified parameters
//
// The params object is updated in place so this method is not thread safe if
// the params are reused
func HashPassword(password string, params *Params) (string, error) {
	var err error
	if params.salt, err = randomBytes(params.SaltLen); err != nil {
		return "", err
	}

	return generateFromPassword(password, params), nil
}

// ValidatePassword validates that a password matches the encoded version
func ValidatePassword(password string, encoded string) (valid bool, err error) {
	var params *Params
	var hash []byte

	params, hash, err = extractArgon2Params(encoded)
	if err != nil {
		return false, err
	}
	return subtle.ConstantTimeCompare(generateHash(password, params), hash) == 1, nil
}

func generateHash(password string, p *Params) []byte {
	hasher := variants[p.Variant]

	return hasher([]byte(password),
		p.salt,
		p.Time,
		p.Memory,
		p.Parallelism,
		p.KeyLen)
}

func generateFromPassword(password string, p *Params) string {
	hash := generateHash(password, p)

	// Base64 encode the salt and hashed password.
	b64Salt := base64.RawStdEncoding.EncodeToString(p.salt)
	b64Hash := base64.RawStdEncoding.EncodeToString(hash)

	return fmt.Sprintf("$argon2i$v=%d$m=%d,t=%d,p=%d$%s$%s", argon2.Version, p.Memory, p.Time, p.Parallelism, b64Salt, b64Hash)
}

func validateVersion(s string) bool {
	var version int
	_, err := fmt.Sscanf(s, "v=%d", &version)
	if err != nil {
		return false
	}
	return version == argon2.Version
}

func parseArguments(s string) (m uint32, t uint32, p uint8, err error) {
	_, err = fmt.Sscanf(s, "m=%d,t=%d,p=%d", &m, &t, &p)
	return
}

func extractArgon2Params(password string) (params *Params, hash []byte, err error) {
	vals := strings.Split(password, "$")

	if len(vals) != 6 {
		return nil, nil, errors.New("invalid component count")
	}

	variant := Variant(vals[1])
	_, ok := variants[variant]
	if !ok {
		return nil, nil, fmt.Errorf("invalid variant %s", variant)
	}

	params = &Params{Variant: variant}

	if !validateVersion(vals[2]) {
		return nil, nil, fmt.Errorf("unsupported version %s", vals[2])
	}

	params.Memory, params.Time, params.Parallelism, err = parseArguments(vals[3])
	if err != nil {
		return nil, nil, fmt.Errorf("could not extract arguments from %s", vals[3])
	}

	params.salt, err = base64.RawStdEncoding.DecodeString(vals[4])
	if err != nil {
		return nil, nil, fmt.Errorf("could not decode salt with err %s", err)
	}

	hash, err = base64.RawStdEncoding.DecodeString(vals[5])
	if err != nil {
		return nil, nil, fmt.Errorf("could not decode hash with err %s", err)
	}
	params.KeyLen = uint32(len(hash))

	return params, hash, err
}
