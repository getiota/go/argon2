module gitlab.com/thegalabs/go/argon2

go 1.16

require (
	github.com/stretchr/testify v1.7.1
	golang.org/x/crypto v0.0.0-20220518034528-6f7dac969898
)
