package argon2

import (
	"encoding/base64"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHashPasswordSanity(t *testing.T) {
	password := "hello world"

	hashed, _ := HashPassword(password, &Params{
		Variant:     Variant2I,
		SaltLen:     16,
		Memory:      32 * 1024,
		Time:        2,
		Parallelism: 2,
		KeyLen:      32,
	})

	vals := strings.Split(hashed, "$")

	params, hash, err := extractArgon2Params(hashed)

	assert.Nil(t, err)
	assert.NotNil(t, params)
	assert.Equal(t, base64.RawStdEncoding.EncodeToString(hash), vals[len(vals)-1])

	valid, _ := ValidatePassword(password, hashed)
	assert.True(t, valid)
}

func TestValidateVersion(t *testing.T) {
	assert.True(t, validateVersion("v=19"))

	falses := []string{"19", "v=18"}
	for i := 0; i < len(falses); i++ {
		assert.False(t, validateVersion(falses[i]))
	}
}

func TestParseArguments(t *testing.T) {
	m, time, p, e := parseArguments("m=65536,t=3,p=2")

	assert.Equal(t, m, uint32(65536))
	assert.Equal(t, time, uint32(3))
	assert.Equal(t, p, uint8(2))
	assert.Nil(t, e)
}

func TestExtractParams(t *testing.T) {
	pwd := "$argon2i$v=19$m=32768,t=5,p=3$4HzdmGurdmmHepfODJ3zWA$Ys4EFauH0a5M4TM8s4u+wpAUC5oyJIMWfMGOT58aEdo" // nolint: gosec

	params, hash, err := extractArgon2Params(pwd)

	assert.Nil(t, err)
	assert.Equal(t, params.Variant, Variant2I)
	assert.Equal(t, params.KeyLen, uint32(32))
	assert.Equal(t, params.Memory, uint32(32768))
	assert.Equal(t, params.Time, uint32(5))
	assert.Equal(t, params.Parallelism, uint8(3))
	assert.Equal(t, base64.RawStdEncoding.EncodeToString(params.salt), "4HzdmGurdmmHepfODJ3zWA")
	assert.Equal(t, base64.RawStdEncoding.EncodeToString(hash), "Ys4EFauH0a5M4TM8s4u+wpAUC5oyJIMWfMGOT58aEdo")
}

func TestValidatePassword(t *testing.T) {
	pwd := "$argon2i$v=19$m=32768,t=2,p=2$4HzdmGurdmmHepfODJ3zWA$Ys4EFauH0a5M4TM8s4u+wpAUC5oyJIMWfMGOT58aEdo" // nolint: gosec
	valid, err := ValidatePassword("hello world", pwd)
	assert.True(t, valid)
	assert.Nil(t, err)

	valid, err = ValidatePassword("hello, world", pwd)
	assert.False(t, valid)
	assert.Nil(t, err)

	pwd = "$argon2i$v=19$m=32768,t=2,p=2$4HzdmGurdmmHepfODJ3zWAYs4EFauH0a5M4TM8s4u+wpAUC5oyJIMWfMGOT58aEdo" // nolint: gosec
	valid, err = ValidatePassword("hello world", pwd)
	assert.False(t, valid)
	assert.NotNil(t, err)
}

func BenchmarkHashPassword(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, _ = HashPassword("Hello12345", &Params{
			Variant:     Variant2I,
			SaltLen:     16,
			Memory:      32 * 1024,
			Time:        2,
			Parallelism: 2,
			KeyLen:      32,
		})
	}
}
