package argon2

import (
	"crypto/rand"
)

// RandomBytes generates an array of n random bytes
func randomBytes(n uint32) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	return b, err
}
